package com.eza.server.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.eza.server.EzaApplication;
import com.eza.server.model.User;

/**
 * 
 * @author emoleumassi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = EzaApplication.class)
public class LoginControllerTest extends AbstractControllerTest {

	private MockMvc mockMvc;
	@InjectMocks
	LoginController loginController;
	private final static String DEFAULT_PATH = "/login";
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginControllerTest.class);

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.addFilter(springSecurityFilterChain)
				.build();
	}

	@Test
	public void loginTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH)
				.param("username", "emo")
				.param("password", "password")
				.header("eza-apikey", propertyConfiguration.getPasswordAuthorization())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.access_token", is(notNullValue())))
				.andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
				.andExpect(jsonPath("$.scope", is(equalTo("login"))))
				.andExpect(jsonPath("$.refresh_token", is(notNullValue())))
				.andExpect(jsonPath("$.expires_in", is(lessThan(100))));
	}

	@Test
	public void badUsernameTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH)
				.param("username", "xxxxx")
				.param("password", "passwordeza")
				.header("eza-apikey", propertyConfiguration.getPasswordAuthorization())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void badPasswordTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH)
				.param("username", "emo")
				.param("password", "xxxxxxxxx")
				.header("eza-apikey", propertyConfiguration.getPasswordAuthorization())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void userNameNoMatchesWithPasswordTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH)
				.param("username", "emo")
				.param("password", "passwordeza")
				.header("eza-apikey", propertyConfiguration.getPasswordAuthorization())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void badEzaApiKeyTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH)
				.param("username", "emo")
				.param("password", "passwordeza")
				.header("eza-apikey", "sxsxsxsxsxxsxsxs")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized());
	}
	
	
	@Test
	public void userEnabledTest() throws Exception {

		mockMvc.perform(post(DEFAULT_PATH)
				.param("username", "usernoenabledtest")
				.param("password", "password")
				.header("eza-apikey", propertyConfiguration.getPasswordAuthorization())
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized());
	}
}
