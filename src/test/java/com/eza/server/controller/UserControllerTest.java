package com.eza.server.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.eza.server.EzaApplication;

/**
 * 
 * @author emoleumassi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = EzaApplication.class)
public class UserControllerTest extends AbstractControllerTest {

	private MockMvc mockMvc;
	@InjectMocks
	UserController userController;
	final static String DEFAULT_PATH = "/users";
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
	private FilterChainProxy springSecurityFilterChain;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserControllerTest.class);
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.addFilter(springSecurityFilterChain).build();
		try {
			accessToken = getAccessToken(mockMvc, ParameterEnum.CLIENT_CREDENTIALS);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	@Test
	public void usersUnauthorized() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized())
				.andExpect(jsonPath("$.error", is("unauthorized")))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test
	public void getUsersTest() throws Exception {

		String content = mockMvc.perform(get(DEFAULT_PATH)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].role.name", is("CUSTOMER")))
				.andExpect(jsonPath("$[0].userUUId", instanceOf(String.class)))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test 
	public void findUserByNameTest() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH + "/byemailorname/emo/")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(equalTo("emo"))))
				.andExpect(jsonPath("$.email", is(equalTo("emoleumassi@gmail.com"))))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test 
	public void findUserByEmailTest() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH + "/byemailorname/emoleumassi@gmail.com/")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(equalTo("emo"))))
				.andExpect(jsonPath("$.email", is(equalTo("emoleumassi@gmail.com"))))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test 
	public void findUserByEmailWithBadEmailTest() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/byemailorname/bademail/")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}
	
	@Test 
	public void findUserByEmailWithBadNameTest() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/byemailorname/badname/")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void forgetUserPasswordWithUsernameTest() throws Exception {
		String content = mockMvc.perform(post(DEFAULT_PATH + "/password/forget")
				.param("identify", "emo")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test
	public void forgetUserPasswordWithEmailTest() throws Exception {
		String content = mockMvc.perform(post(DEFAULT_PATH + "/password/forget")
				.param("identify", "emoleumassi@gmail.com")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test
	public void forgetUserPasswordWithBadNameTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH + "/password/forget")
				.param("identify", "badname")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void forgetUserPasswordWithUserNotEnabledTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH + "/password/forget")
				.param("identify", "usernoenabledtest")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void handlePasswordTokenTest() throws Exception {
		
		String verificationToken = findVerificationToken();
		
		mockMvc.perform(get(DEFAULT_PATH + "/password/handleverificationtoken?verificationToken=" + verificationToken)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk());
	}
	
	@Test
	public void handlePasswordTokenWithBadTokenTest() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/password/handleverificationtoken?verificationToken=badverificationToken")
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}

	@Ignore
	@Test
	public void resetPasswordTest() throws Exception {
		String verificationToken = findVerificationToken();
		mockMvc.perform(post(DEFAULT_PATH + "/password/reset")
				.param("verificationToken", verificationToken)
				.param("password", "newpassword")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk());
	}
	
	@Test
	public void resetPasswordWithBadVerificationTokenTest() throws Exception {
		mockMvc.perform(post(DEFAULT_PATH + "/password/reset")
				.param("verificationToken", "badverificationToken")
				.param("password", "badpassword")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}
	
	private String findVerificationToken() {
		String query = "SELECT rt.verificationToken FROM RegistrationToken rt, User u "
				+ "WHERE u.userUUId = rt.user AND u.name = :name";
		return (String) entityManager.createQuery(query)
				.setParameter("name", "emo").getSingleResult();
	}
}
