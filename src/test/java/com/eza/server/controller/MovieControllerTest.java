package com.eza.server.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import com.eza.server.service.GlobalEntityManagerService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.eza.server.EzaApplication;
import com.eza.server.model.Crew;
import com.eza.server.model.CrewTranslation;
import com.eza.server.model.Film;
import com.eza.server.model.Movie;
import com.eza.server.model.MovieTranslation;
import com.eza.server.model.Season;
import com.eza.server.model.Serie;
import com.eza.server.model.noentities.MovieCategory;
import com.eza.server.model.noentities.MovieFormat;

/**
 * 
 * @author emoleumassi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = EzaApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MovieControllerTest extends AbstractControllerTest {

	@InjectMocks
	Crew crew;
	@InjectMocks
	Movie movie;
	@InjectMocks
	Film film;
	@InjectMocks
	Season season;
	@InjectMocks
	Serie serie;
	@InjectMocks
	CrewTranslation crewTranslation;
	@InjectMocks
	MovieTranslation movieTranslation;
	private MockMvc mockMvc;
	@InjectMocks
	MovieController movieController;
	private final static String DEFAULT_PATH = "/movies";
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private static String title;

	@Autowired
	GlobalEntityManagerService globalEntityManagerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(MovieControllerTest.class);

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
				.build();
		try {
			accessToken = getAccessToken(mockMvc, ParameterEnum.CLIENT_CREDENTIALS);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}

	@Test
	public void moviesUnauthorized() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized())
				.andExpect(jsonPath("$.error", is("unauthorized")))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}

	// Get all serie with season and movie translation.
	@Test
	public void moviesTest() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}

	//create serie
	@Test
	public void serieTest() throws Exception {

		title = randomString();
		MovieFormat[] movieFormats = createMovieFormats();
		MovieCategory[] movieCategories = createMovieCategories();
		
		serie.setTitle(title);
		serie.setMovieCategory(movieCategories);
		
		createCrew("Crew: " + randomString());
		
		season.setCrew(Collections.singletonList(crew));
		season.setSerie(serie);
		season.setTitle(title + " season");
		serie.setSeasons(Collections.singletonList(season));

		movie.setSeason(season);
		movie.setDuration(100000);
		movie.setMpd(randomString());
		movie.setNumberOfView(1234);
		movie.setCreationDate("2016");
		movie.setMovieFormat(movieFormats);
		movie.setMpdTrailer(randomString() + " trailer");
		season.setMovies(Collections.singletonList(movie));

		movieTranslation.setMovie(movie);
		movieTranslation.setTitle(title + " mt");
		movieTranslation.setLanguage("EN");
		movieTranslation.setDescription("Test Description");
		movie.setMovieTranslations(Collections.singletonList(movieTranslation));

		String content = mockMvc.perform(post(DEFAULT_PATH + "/create")
				.content(asJsonString(serie))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.serieUUId", notNullValue()))
				.andExpect(jsonPath("$.seasons[0].seasonUUId", notNullValue()))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
		String uuid = content.substring(14, 50);
		LOGGER.info("the uuid of the new serie is: " + uuid);
	}

	// create season to serie
	@Test
	public void serieTest1() throws Exception {

		Serie serie = findSerieByTitle(title);
		MovieFormat[] movieFormats = createMovieFormats();

		createCrew("Crew: " + randomString());

		season.setCrew(Collections.singletonList(crew));
		season.setSerie(serie);
		season.setTitle(title + " season 1");
		serie.setSeasons(Collections.singletonList(season));

		movie.setSeason(season);
		movie.setDuration(2500);
		movie.setMpd(randomString());
		movie.setNumberOfView(500);
		movie.setCreationDate("2016");
		movie.setMovieFormat(movieFormats);
		movie.setMpdTrailer(randomString() + " trailer");
		season.setMovies(Collections.singletonList(movie));

		movieTranslation.setMovie(movie);
		movieTranslation.setTitle(title + " mt 1");
		movieTranslation.setLanguage("FR");
		movieTranslation.setDescription("Test Description");
		movie.setMovieTranslations(Collections.singletonList(movieTranslation));

		handlePostRequest(season, "/createSeason/" + title);
	}

	// create movie to season
	@Test
	public void serieTest2() throws Exception {

		Season season = findSeasonByTitle(title + " season 1");
		MovieFormat[] movieFormats = createMovieFormats();

		movie.setSeason(season);
		movie.setDuration(2500);
		movie.setMpd(randomString());
		movie.setNumberOfView(500);
		movie.setCreationDate("2016");
		movie.setMovieFormat(movieFormats);
		movie.setMpdTrailer(randomString() + " trailer");
		season.setMovies(Collections.singletonList(movie));

		movieTranslation.setMovie(movie);
		movieTranslation.setTitle(title + " mt 2");
		movieTranslation.setLanguage("FR");
		movieTranslation.setDescription("Test Description");
		movie.setMovieTranslations(Collections.singletonList(movieTranslation));

		handlePostRequest(movie, "/createMovie/" + title + "/" + title + " season 1");
	}

	// find serie by title
	@Test
	public void serieTest3() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is(equalTo(title))));
	}

	// find serie by wrong title
	@Test
	public void serieTest3_1() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/blabla")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}

	// find season by serie's and season's title
	@Test
	public void serieTest4() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("seasontitle", title + " season")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is(equalTo(title + " season"))));
	}

	// find season by serie's and wrong season's title
	@Test
	public void serieTest4_1() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("seasontitle", "blabla")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}

	// find movie by serie's, season's and movie's title
	@Test
	public void serieTest5() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("seasontitle", title + " season")
				.param("movietranslationtitle", title + " mt")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk());
				//.andExpect(jsonPath("$movieTranslations[0].title", is(equalTo(title + " mt"))))
	}

	// find movie by serie's, season's and wrong movie's title
	@Test
	public void serieTest5_1() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("seasontitle", title + " season")
				.param("movietranslationtitle", "blabla")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}

	// find movie by serie's, wrong season's and wrong movie's title
	@Test
	public void serieTest5_1_1() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("seasontitle", "blabla")
				.param("movietranslationtitle", "blabla")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}

	// find movie by serie's, wrong season's and movie's title
	@Test
	public void serieTest5_1_2() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("seasontitle", "blabla")
				.param("movietranslationtitle", title + " mt")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}

	// find movie by serie's and movie's title. Bad request.
	// client has to set a season title
	@Test
	public void serieTest5_1_3() throws Exception {
		mockMvc.perform(get(DEFAULT_PATH + "/" + title)
				.param("movietranslationtitle", title + " mt")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}

	private void handlePostRequest(Object object, String path) throws Exception {
		String content = mockMvc.perform(post(DEFAULT_PATH + path)
				.content(asJsonString(object))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}

	private Serie findSerieByTitle(String title){
		globalEntityManagerService.setClazz(Serie.class);
		return (Serie) globalEntityManagerService.findEntityByParameter("title", title);
	}

	private Season findSeasonByTitle(String title){
		globalEntityManagerService.setClazz(Season.class);
		return (Season) globalEntityManagerService.findEntityByParameter("title", title);
	}

	private MovieFormat[] createMovieFormats() {
		MovieFormat[] movieFormats = new MovieFormat[1];
		movieFormats[0] = new MovieFormat();
		movieFormats[0].setFormat("Test movie format");
		return movieFormats;
	}

	private MovieCategory [] createMovieCategories() {
		MovieCategory[] movieCategories = new MovieCategory[1];
		movieCategories[0] = new MovieCategory();
		movieCategories[0].setCategory("Full HD");
		return movieCategories;
	}

	private void createCrew(String name){
		crew.setName(name);
		crewTranslation.setCrew(crew);
		crewTranslation.setLanguage("EN");
		crewTranslation.setDescription("this is ...");
		crew.setCrewTranslations(Collections.singletonList(crewTranslation));
	}
}
