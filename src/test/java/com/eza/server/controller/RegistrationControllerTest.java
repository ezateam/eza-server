package com.eza.server.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.eza.server.EzaApplication;
import com.eza.server.model.User;
import com.eza.server.model.noentities.RegistrationUser;

/**
 * 
 * @author emoleumassi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = EzaApplication.class)
public class RegistrationControllerTest extends AbstractControllerTest {
	
	private MockMvc mockMvc;
	@PersistenceContext
	EntityManager entityManager;
	@InjectMocks
	RegistrationUser registrationUser;
	@InjectMocks
	RegistrationController registrationController;
	final static String DEFAULT_PATH = "/registration";
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationControllerTest.class);

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.addFilter(springSecurityFilterChain)
				.build();
		try {
			accessToken = getAccessToken(mockMvc, ParameterEnum.CLIENT_CREDENTIALS);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}

	@Test
	public void registrationUnauthorized() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized())
				.andExpect(jsonPath("$.error", is("unauthorized")))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test
	public void registrationBadAuthorization() throws Exception {
		String content = mockMvc.perform(get(DEFAULT_PATH)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer BadAuthorization"))
				.andExpect(status().isUnauthorized())
				.andExpect(jsonPath("$.error", is("invalid_token")))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test
	public void registrerUserTest () throws Exception {
		
		String randomString = randomString();
		registrationUser.setRole("CUSTOMER");
		registrationUser.setUsername(randomString);
		registrationUser.setPassword("unittest password");
		registrationUser.setEmail(randomString + "@unittest.de");
		
		String content = mockMvc.perform(post(DEFAULT_PATH + "/newuser")
				.content(asJsonString(registrationUser))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.enabled", is(false)))
				.andExpect(jsonPath("$.userUUId", notNullValue()))
				.andExpect(jsonPath("$.name", is(randomString)))
				.andExpect(jsonPath("$.email", is(randomString + "@unittest.de")))
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
		
		String uuid = content.substring(13, 49);
		LOGGER.info("the uuid of the new user is: " + uuid);
		
		String query = "SELECT rt.verificationToken FROM RegistrationToken rt, User u WHERE "
				+ "u.userUUId = rt.user AND u.userUUId = :userUUId";
		String verificationToken = (String) entityManager.createQuery(query).setParameter("userUUId", uuid)
				.getSingleResult();
		assertNotNull("The verificationToken hasn't been null", verificationToken);
		LOGGER.info("the verificationToken of the new user is: " + verificationToken);
		
		
		mockMvc.perform(get(DEFAULT_PATH + "/confirmRegistration?verificationToken=" + verificationToken)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk());
	}

	@Test
	public void sendNewVerificationTokenTest() throws Exception {

		String content = mockMvc.perform(post(DEFAULT_PATH + "/sendNewVerificationToken")
				.param("identify", "usernoenabledtest")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}

	@Test
	public void sendNewVerificationTokenIdentifyDoesntExistsTest() throws Exception {

		String content = mockMvc.perform(post(DEFAULT_PATH + "/sendNewVerificationToken")
				.param("identify", "userwithoutVT@gmail.com")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound())
				.andReturn().getResponse().getContentAsString();
		LOGGER.info(content);
	}
	
	@Test
	public void checkBadVerificationTokenTest() throws Exception {
		
		mockMvc.perform(get(DEFAULT_PATH + "/confirmationRegistration?verificationToken=badverificationToken")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isNotFound());
	}

	@Test
	public void registrerUserWithUsernameAlreadyExistsTest () throws Exception {
		
		String randomString = randomString();
		registrationUser.setRole("CUSTOMER");
		registrationUser.setUsername("emo");
		registrationUser.setPassword("passwordeza");
		registrationUser.setEmail(randomString + "@unittest.de");
		
		mockMvc.perform(post(DEFAULT_PATH + "/newuser")
				.content(asJsonString(registrationUser))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void registrerUserWithEmailAlreadyExistsTest () throws Exception {
		
		String randomString = randomString();
		registrationUser.setRole("CUSTOMER");
		registrationUser.setUsername(randomString);
		registrationUser.setPassword("passwordeza");
		registrationUser.setEmail("emoleumassi@gmail.com");
		
		mockMvc.perform(post(DEFAULT_PATH + "/newuser")
				.content(asJsonString(registrationUser))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isBadRequest());
	}
}
