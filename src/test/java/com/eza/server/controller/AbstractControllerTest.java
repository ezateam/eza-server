package com.eza.server.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.eza.server.bootstrap.PropertyConfiguration;
import com.eza.server.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author emoleumassi
 *
 */
public class AbstractControllerTest {
	
	String accessToken;
	@PersistenceContext
	EntityManager entityManager;
	@Autowired
	PropertyConfiguration propertyConfiguration;
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractControllerTest.class);
	
	protected String getAccessToken(MockMvc mockMvc, ParameterEnum parameterEnum) throws Exception {
		
		String clientId, clientSecret;
		MultiValueMap<String, String> parameters;
		
		switch (parameterEnum) {
		case PASSWORD:
			clientId = propertyConfiguration.getPasswordId();
			clientSecret = propertyConfiguration.getPasswordSecret();
			parameters = buildPasswordParameters(clientId, clientSecret);
			break;
		default:
			clientId = propertyConfiguration.getClientCredentialsId();
			clientSecret = propertyConfiguration.getClientCredentialsSecret();
			parameters = buildClientCredentialsParameters(clientId, clientSecret);
			break;
		}
		
		String authorization = "Basic "
				+ new String(Base64Utils.encode((clientId + ":" + clientSecret).getBytes()));
		String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";

		String content = mockMvc
				.perform(
						post("/oauth/token")
								.header("Authorization", authorization)
								.contentType(
										MediaType.APPLICATION_FORM_URLENCODED)
								.params(parameters))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.access_token", is(notNullValue())))
				.andExpect(jsonPath("$.token_type", is(equalTo("bearer"))))
				.andExpect(jsonPath("$.expires_in", is(lessThan(301))))
				.andReturn().getResponse().getContentAsString();
		return content.substring(17, 53);
	}
	
	String asJsonString(final Object obj) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	String randomString(){
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}
	
	MultiValueMap<String, String> buildPasswordParameters(String clientId, String clientSecret){
		
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("username", "emo");
		parameters.add("password", "password");
		parameters.add("grant_type", "password");
		parameters.add("scope", "login");
		parameters.add("client_id", clientId);
		parameters.add("client_secret", clientSecret);
		return parameters;
	}
	
	MultiValueMap<String, String> buildClientCredentialsParameters(String clientId, String clientSecret){
		
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("grant_type", "client_credentials");
		parameters.add("scope", "read write delete");
		parameters.add("client_id", clientId);
		parameters.add("client_secret", clientSecret);
		return parameters;
	}
}
