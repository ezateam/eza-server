package com.eza.server.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.eza.server.EzaApplication;

/**
 * 
 * @author emoleumassi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = EzaApplication.class)
public class RoleControllerTest extends AbstractControllerTest {

	private MockMvc mockMvc;
	@InjectMocks
	RoleController roleController;
	final static String DEFAULT_PATH = "/roles";
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleControllerTest.class);
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.addFilter(springSecurityFilterChain).build();
		try {
			accessToken = getAccessToken(mockMvc, ParameterEnum.CLIENT_CREDENTIALS);
		} catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}
	
	@Test
	public void rolesAuthorized() throws Exception {

		mockMvc.perform(get(DEFAULT_PATH)
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].roleId", is(1)))
				.andExpect(jsonPath("$[0].name", is("ADMIN")))
				.andReturn().getResponse().getContentAsString();
		
//		mockMvc.perform(get(DEFAULT_PATH)
//				.header("Authorization", "Bearer " + accessToken))
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$[1].roleId", is(2)))
//				.andExpect(jsonPath("$[1].name", is("PRODUCER")))
//				.andReturn().getResponse().getContentAsString();
//		
//		mockMvc.perform(get(DEFAULT_PATH)
//				.header("Authorization", "Bearer " + accessToken))
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$[2].roleId", is(3)))
//				.andExpect(jsonPath("$[2].name", is("CUSTOMER")))
//				.andReturn().getResponse().getContentAsString();
	}
	
	@Test
	public void roleNameAuthorized() throws Exception {
		
		mockMvc.perform(get(DEFAULT_PATH + "/ADMIN")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.roleId", is(1)))
				.andExpect(jsonPath("$.name", is("ADMIN")))
				.andReturn().getResponse().getContentAsString();
		
		mockMvc.perform(get(DEFAULT_PATH + "/PRODUCER")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.roleId", is(2)))
				.andExpect(jsonPath("$.name", is("PRODUCER")))
				.andReturn().getResponse().getContentAsString();
		
		mockMvc.perform(get(DEFAULT_PATH + "/CUSTOMER")
				.header("Authorization", "Bearer " + accessToken))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.roleId", is(3)))
				.andExpect(jsonPath("$.name", is("CUSTOMER")))
				.andReturn().getResponse().getContentAsString();
	}
}
