package com.eza.server.security;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.eza.server.bootstrap.PropertyConfiguration;

/**
 * 
 * @author emoleumasi
 *
 */
@Service("endpointsAuthentification")
public class EndpointsAuthentification {

	@Autowired
	PropertyConfiguration propertyConfiguration;

	private static final String OAUTH_TOKEN = "/oauth/token";
	private static final Logger LOGGER = LoggerFactory.getLogger(EndpointsAuthentification.class);

	public final ResponseEntity<String> login(HttpEntity<?> httpEntity) {

		String url = propertyConfiguration.getEzaHost() + OAUTH_TOKEN;
		ResponseEntity<String> responseEntity = null;
		try {
			disableCertificateVerification();
			RestTemplate restTemplate = new RestTemplate();
			responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		} catch (HttpClientErrorException e) {
			LOGGER.error(e.toString());
		}
		return responseEntity;
	}

	private void disableCertificateVerification() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
		} };

		try {
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(
					(hostname, session) -> hostname.equals(propertyConfiguration.getEzaIPAddress()));
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			LOGGER.error(e.toString());
		}
	}
}
