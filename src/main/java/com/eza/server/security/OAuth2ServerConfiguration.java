package com.eza.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import com.eza.server.bootstrap.PropertyConfiguration;

/**
 * 
 * @author emoleumassi
 *
 */
@Configuration
public class OAuth2ServerConfiguration {
	
	private static final String SCOPE_READ = "read";
	private static final String SCOPE_WRITE = "write";
	private static final String SCOPE_DELETE = "delete";
	private static final String SCOPE_LOGIN = "login";
	
	private static final String ROLE_PRODUCER = "PRODUCER";
	
	private static final String OAUTH_SCOPE_READ = "#oauth2.hasScope('" + SCOPE_READ + "')";
	private static final String OAUTH_SCOPE_WRITE = "#oauth2.hasScope('" + SCOPE_WRITE + "')";
	private static final String OAUTH_SCOPE_DELETE = "#oauth2.hasScope('" + SCOPE_DELETE + "')";
	private static final String OAUTH_SCOPE_LOGIN = "#oauth2.hasScope('" + SCOPE_LOGIN + "')";
	
	private static final String RESOURCE_ID = "restservice";

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId(RESOURCE_ID);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {

			String readURLs[] = {"/movies", "/movies/newrecords/**"};
			String loginURLs[] = {"/usersession/**"};
			String deleteURLs[] = {"/movies/delete/**", "/usersession/delete/**"};
			String writeURLs[] = {"/movies/**", "/roles/**", "/registration/**", "/users/**"};
			
			http.authorizeRequests()
				.antMatchers(writeURLs)
				.access(OAUTH_SCOPE_WRITE)
				.antMatchers(deleteURLs)
				.access(OAUTH_SCOPE_DELETE)
				.antMatchers(readURLs)
				.access(OAUTH_SCOPE_READ)
				.antMatchers(loginURLs)
				.access(OAUTH_SCOPE_LOGIN)
				.antMatchers("/testeza")
				.authenticated()
				.and()
				.authorizeRequests()
				.antMatchers("/test")
				.access("#oauth2.hasScope('read')");
		}

	}

	@Configuration
	@EnableAuthorizationServer
	@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Bean(name = "tokenStore")
        public TokenStore tokenStore() {
            return new InMemoryTokenStore();
        }

		@Autowired
		private PropertyConfiguration propertyConfiguration;

		@Autowired
		private CustomUserDetailsService customUserDetailsService;

		@Autowired
		private AuthenticationManager authenticationManager;

		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			endpoints
				.tokenStore(tokenStore())
				.authenticationManager(authenticationManager)
				.userDetailsService(customUserDetailsService);
		}

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.inMemory()
				.withClient(propertyConfiguration.getPasswordId())
				.authorizedGrantTypes("password", "refresh_token")
				.authorities(ROLE_PRODUCER)
				.scopes(SCOPE_LOGIN)
				.resourceIds(RESOURCE_ID)
				.secret(propertyConfiguration.getPasswordSecret())
				.accessTokenValiditySeconds(60)
			.and()
				.withClient(propertyConfiguration.getClientCredentialsId())
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.authorities(ROLE_PRODUCER)
				.scopes(SCOPE_READ, SCOPE_WRITE, SCOPE_DELETE)
				.resourceIds(RESOURCE_ID)
				.secret(propertyConfiguration.getClientCredentialsSecret())
				.accessTokenValiditySeconds(300);
		}
	
		@Bean
		@Primary
		public DefaultTokenServices tokenServices() {
			DefaultTokenServices tokenServices = new DefaultTokenServices();
			tokenServices.setSupportRefreshToken(true);
			tokenServices.setTokenStore(this.tokenStore());
			return tokenServices;
		}
	}
}