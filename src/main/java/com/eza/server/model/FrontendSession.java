package com.eza.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class FrontendSession implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", columnDefinition = "VARCHAR(32)")
	private String sessionUUId;

	@Column(columnDefinition = "INTEGER")
	private int access;

	@Column(columnDefinition = "TEXT")
	private String data;

	@Column
	private int lifetime;
	
	public String getSessionUUId() {
		return sessionUUId;
	}

	public void setSessionUUId(String sessionUUId) {
		this.sessionUUId = sessionUUId;
	}

	public int getAccess() {
		return access;
	}

	public void setAccess(int access) {
		this.access = access;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getLifetime() {
		return lifetime;
	}

	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}
}
