package com.eza.server.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
/**
 * 
 * @author emoleumassi
 *
 */

import com.eza.server.model.noentities.MovieCategory;

@Entity
@Table
public class Film implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String filmUUId;

	@NotEmpty
	@Column(columnDefinition = "TEXT", nullable = false)
	private Crew[] crew;

	@NotEmpty
	@Column(columnDefinition = "TEXT", nullable = false)
	private MovieCategory[] movieCategory;

//	@JsonBackReference
//	@OneToOne(targetEntity = Movie.class, fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
//			CascadeType.REMOVE })
//	@JoinColumn(name = "movieUUId", referencedColumnName = "id")
//	private Movie movie;

	public String getFilmUUId() {
		return filmUUId;
	}

	public void setFilmUUId(String filmUUId) {
		this.filmUUId = filmUUId;
	}

	public Crew[] getCrew() {
		return crew;
	}

	public void setCrew(Crew[] crew) {
		this.crew = crew;
	}

	public MovieCategory[] getMovieCategory() {
		return movieCategory;
	}

	public void setMovieCategory(MovieCategory[] movieCategory) {
		this.movieCategory = movieCategory;
	}

//	public Movie getMovie() {
//		return movie;
//	}
//
//	public void setMovie(Movie movie) {
//		this.movie = movie;
//	}
}
