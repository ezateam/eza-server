package com.eza.server.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class MovieTranslation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String movieTranslationUUId;

	@NotNull
	@Column(columnDefinition = "VARCHAR(50)", nullable = false)
	private String title;

	@NotEmpty
	@Column(columnDefinition = "CHAR(2)", nullable = false)
	private String language;

	@NotBlank
	@Column(columnDefinition = "TEXT", nullable = false)
	private String description;

	@JsonBackReference
	@ManyToOne(targetEntity = Movie.class, fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	@JoinColumn(name = "movieUUId", referencedColumnName = "id")
	private Movie movie;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMovieTranslationUUId() {
		return movieTranslationUUId;
	}

	public void setMovieTranslationUUId(String movieTranslationUUId) {
		this.movieTranslationUUId = movieTranslationUUId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}
}
