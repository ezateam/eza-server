package com.eza.server.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String userUUId;

	@Column(name = "username", unique = true, columnDefinition = "VARCHAR(50)", nullable = false)
	private String name;

	@Column(nullable = false, unique = true, columnDefinition = "VARCHAR(50)")
	private String email;

	@Column(nullable = false, columnDefinition = "TINYINT DEFAULT false")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean enabled;

	@Column(columnDefinition = "VARCHAR(50)", nullable = false)
	private String password;

	@ManyToOne(targetEntity = Role.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId", referencedColumnName = "id")
	private Role role;

	@JsonIgnore
	@Transient
	private Set<Role> roles;

	public User() {
	}

	public User(User user) {
		super();
		this.userUUId = user.getUserUUId();
		this.roles = user.getRoles();
		this.name = user.getName();
		this.email = user.getEmail();
		this.enabled = user.isEnabled();
		this.password = user.getPassword();
	}

	public String getUserUUId() {
		return userUUId;
	}

	public String getName() {
		return name;
	}

	public void setUsername(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
}