package com.eza.server.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class Crew implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String crewUUId;

	@NotNull
	@Column(columnDefinition = "VARCHAR(50)", unique = true, nullable = false)
	private String name;
	
	@JsonBackReference
	@ManyToOne(targetEntity = Season.class, fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	@JoinColumn(name = "seasonUUId", referencedColumnName = "id")
	private Season season;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "crew", targetEntity = CrewTranslation.class, fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	private List<CrewTranslation> crewTranslations = new LinkedList<>();

	public String getCrewUUId() {
		return crewUUId;
	}

	public void setCrewUUId(String crewUUId) {
		this.crewUUId = crewUUId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public List<CrewTranslation> getCrewTranslations() {
		return crewTranslations;
	}

	public void setCrewTranslations(List<CrewTranslation> crewTranslations) {
		this.crewTranslations = crewTranslations;
	}
}
