package com.eza.server.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class Season implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String seasonUUId;

	@NotNull
	@Column(columnDefinition = "VARCHAR(50)", nullable = false)
	protected String title;

	@JsonManagedReference
	@OneToMany(mappedBy = "season", targetEntity = Crew.class, fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	private List<Crew> crews = new LinkedList<>();

	@JsonBackReference
	@ManyToOne(targetEntity = Serie.class, fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	@JoinColumn(name = "serieUUId", referencedColumnName = "id")
	private Serie serie;

	@JsonManagedReference
	@OneToMany(mappedBy = "season", targetEntity = Movie.class, fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	private List<Movie> movies = new LinkedList<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSeasonUUId() {
		return seasonUUId;
	}

	public void setSeasonUUId(String seasonUUId) {
		this.seasonUUId = seasonUUId;
	}

	public List<Crew> getCrews() {
		return crews;
	}

	public void setCrew(List<Crew> crews) {
		this.crews = crews;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}
}
