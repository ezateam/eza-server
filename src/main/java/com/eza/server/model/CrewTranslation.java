package com.eza.server.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class CrewTranslation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String crewTranslationUUId;

	@NotEmpty
	@Column(columnDefinition = "CHAR(2)", nullable = false)
	private String language;

	@NotBlank
	@Column(columnDefinition = "TEXT", nullable = false)
	private String description;

	@JsonBackReference
	@ManyToOne(targetEntity = Crew.class, fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	@JoinColumn(name = "crewUUId", referencedColumnName = "id")
	private Crew crew;

	public String getCrewTranslationUUId() {
		return crewTranslationUUId;
	}

	public void setCrewTranslationUUId(String crewTranslationUUId) {
		this.crewTranslationUUId = crewTranslationUUId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Crew getCrew() {
		return crew;
	}

	public void setCrew(Crew crew) {
		this.crew = crew;
	}
}
