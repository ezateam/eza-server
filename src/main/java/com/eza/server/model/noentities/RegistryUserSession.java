package com.eza.server.model.noentities;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 
 * @author emoleumassi
 *
 */
public class RegistryUserSession {

	@NotBlank
	private String sessionId;
	@NotBlank
	private String clientHost;
	@NotBlank
	private String userAgent;
	private String rememberId;
	private String rememberToken;
	@NotBlank
	private String userUUId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getClientHost() {
		return clientHost;
	}

	public void setClientHost(String clientHost) {
		this.clientHost = clientHost;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getRememberId() {
		return rememberId;
	}

	public void setRememberId(String rememberId) {
		this.rememberId = rememberId;
	}

	public String getRememberToken() {
		return rememberToken;
	}

	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}

	public String getUserUUId() {
		return userUUId;
	}

	public void setUserUUId(String userUUId) {
		this.userUUId = userUUId;
	}

	@Override
	public String toString() {
		return "RegistryUserSession [sessionId=" + sessionId + ", clientHost=" + clientHost + ", userAgent=" + userAgent
				+ ", rememberId=" + rememberId + ", rememberToken=" + rememberToken + ", userUUId=" + userUUId + "]";
	}
}
