package com.eza.server.model.noentities;

import java.io.Serializable;

/**
 * 
 * @author emoleumassi
 *
 */
public class MovieFormat implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String format;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return "MovieFormat [format=" + format + "]";
	}
}
