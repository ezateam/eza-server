package com.eza.server.model.noentities;

import java.io.Serializable;

/**
 * 
 * @author emoleumassi
 *
 */
public class MovieCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	private String category;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "MovieCategory [category=" + category + "]";
	}
}
