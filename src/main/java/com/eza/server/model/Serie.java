package com.eza.server.model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import com.eza.server.model.noentities.MovieCategory;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class Serie {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String serieUUId;

	@NotNull
	@Column(columnDefinition = "VARCHAR(50)", unique = true, nullable = false)
	protected String title;

	@NotEmpty
	@Column(columnDefinition = "VARCHAR(255)", nullable = false)
	private MovieCategory[] movieCategory;

	@JsonManagedReference
	@OneToMany(mappedBy = "serie", targetEntity = Season.class, fetch = FetchType.LAZY, cascade = {
			CascadeType.PERSIST, CascadeType.REMOVE })
	private List<Season> seasons = new LinkedList<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSerieUUId() {
		return serieUUId;
	}

	public void setSerieUUId(String serieUUId) {
		this.serieUUId = serieUUId;
	}

	public MovieCategory[] getMovieCategory() {
		return movieCategory;
	}

	public void setMovieCategory(MovieCategory[] movieCategory) {
		this.movieCategory = movieCategory;
	}

	public List<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<Season> seasons) {
		this.seasons = seasons;
	}
}
