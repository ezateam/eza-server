package com.eza.server.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.eza.server.model.noentities.MovieFormat;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * 
 * @author emoleumassi
 *
 */
@Entity
@Table
public class Movie implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "VARCHAR(50)")
	private String movieUUId;

	@NotNull
	@Column(columnDefinition = "BIGINT", nullable = false)
	private long duration;

	@NotNull
	@Column(columnDefinition = "BIGINT")
	private long numberOfView;

	@NotBlank
	@Column(columnDefinition = "CHAR(4)", nullable = false)
	private String creationDate;

	@NotEmpty
	@Column(columnDefinition = "VARCHAR(255)", nullable = false)
	private MovieFormat[] movieFormat;

	@NotBlank
	@Column(columnDefinition = "VARCHAR(20)", unique = true, nullable = false)
	private String mpd;

	@NotBlank
	@Column(columnDefinition = "VARCHAR(20)", unique = true, nullable = false)
	private String mpdTrailer;

	@JsonBackReference
	@ManyToOne(targetEntity = Season.class, fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	@JoinColumn(name = "seasonUUId", referencedColumnName = "id")
	private Season season;

//	@JsonManagedReference
//	@OneToOne(mappedBy = "movie", targetEntity = MovieTranslation.class, fetch = FetchType.LAZY, cascade = {
//			CascadeType.PERSIST, CascadeType.REMOVE })
//	private Film film;

	@JsonManagedReference
	@OneToMany(mappedBy = "movie", targetEntity = MovieTranslation.class, fetch = FetchType.LAZY, cascade = {
			CascadeType.PERSIST, CascadeType.REMOVE })
	private List<MovieTranslation> movieTranslations = new LinkedList<>();

	public String getMovieUUId() {
		return movieUUId;
	}

	public void setMovieUUId(String movieUUId) {
		this.movieUUId = movieUUId;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getNumberOfView() {
		return numberOfView;
	}

	public void setNumberOfView(long numberOfView) {
		this.numberOfView = numberOfView;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public MovieFormat[] getMovieFormat() {
		return movieFormat;
	}

	public void setMovieFormat(MovieFormat[] movieFormat) {
		this.movieFormat = movieFormat;
	}

	public String getMpd() {
		return mpd;
	}

	public void setMpd(String mpd) {
		this.mpd = mpd;
	}

	public String getMpdTrailer() {
		return mpdTrailer;
	}

	public void setMpdTrailer(String mpdTrailer) {
		this.mpdTrailer = mpdTrailer;
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

//	public Film getFilm() {
//		return film;
//	}
//
//	public void setFilm(Film film) {
//		this.film = film;
//	}

	public List<MovieTranslation> getMovieTranslations() {
		return movieTranslations;
	}

	public void setMovieTranslations(List<MovieTranslation> movieTranslations) {
		this.movieTranslations = movieTranslations;
	}
}
