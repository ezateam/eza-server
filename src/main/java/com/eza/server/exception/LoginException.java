package com.eza.server.exception;

/**
 * Created by emoleumassi on 25.08.16.
 */
public class LoginException extends RuntimeException {

    public LoginException(String message){
        super(message);
    }

    public LoginException(String message, Throwable throwable){
        super(message, throwable);
    }
}
