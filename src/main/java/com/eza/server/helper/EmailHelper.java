package com.eza.server.helper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.eza.server.model.RegistrationToken;

/**
 * 
 * @author emoleumassi
 *
 */
@Service("emailHelper")
public class EmailHelper {

	public enum EmailType {
		NEW_USER, FORGET_PASSWORD
	}

	private static final String WELCOME_SUBJECT = "Bienvenu chez EZA ";
	private static final String FORGET_PASSWORD_SUBJECT = "Reinitialise ton mot de passe sur EZA ";

	private static final String HOST = "http://52.28.205.249/";
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailHelper.class);

	@Autowired
	private JavaMailSender javaMailSender;

	public void sentEmail(RegistrationToken registrationToken, EmailType emailType) {

		String subject;
		String message;
		String url = HOST;
		String urlText;
		String username = registrationToken.getUser().getName();
		String recipientAddress = registrationToken.getUser().getEmail();
		String ezaLink = "<a href=\"" + HOST + "\"> eza.com</a>";

		switch (emailType) {
		case NEW_USER:
			url += "register/confirm/";
			urlText = "Activer votre compte";
			subject = WELCOME_SUBJECT + username;
			message = "Vous venez de créer un compte sur " + ezaLink
					+ "Pour activer votre compte il vous suffit de cliquer sur le button ci-dessous.";
			break;
		default:
			url += "resetpassword/";
			urlText = "Reinitialiser votre mot de passe";
			subject = FORGET_PASSWORD_SUBJECT + username;
			message = "Vous souhaitez reinitialiser votre mot de passe sur " + ezaLink
					+ "Cliquez sur le button ci-dessous.";
			break;
		}

		url += registrationToken.getVerificationToken() + "?identify=" + username;

		String customMessage = "<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b --><!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><meta name=\"viewport\" content=\"width=device-width\" /><link href=\"//fonts.googleapis.com/css?family=Raleway:400,300,600\" rel=\"stylesheet\" type=\"text/css\"><link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'></head>"
				+ "<body style=\"width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\"><table class=\"body\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\">"
				+ "<tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td class=\"center\" align=\"center\" valign=\"top\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\"><center style=\"width: 100%; min-width: 580px;\"><table class=\"row header\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; background: #fff; padding: 0px;\" bgcolor=\"#fff\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td class=\"center\" align=\"center\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\" valign=\"top\"><center style=\"width: 100%; min-width: 580px;\"><table class=\"container\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; margin: 0 auto; padding: 0;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td class=\"wrapper last\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 10px 0px 0px;\" align=\"left\" valign=\"top\"><br /><table class=\"twelve columns\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td style=\"text-align: center; vertical-align: middle; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;\" align=\"center\" valign=\"middle\"><span style=\"font-family: Bangers,Helvetica,Arial,sans-serif; font-weight: 700; font-size: 50px; color: #4f5b66;\">EZA Streaming</span></td></tr></table></td></tr></table></center></td></tr></table><table class=\"container\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; margin: 0 auto; padding: 0;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\" align=\"left\" valign=\"top\"><table class=\"row\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td class=\"wrapper last\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 10px 0px 0px;\" align=\"left\" valign=\"top\"><table class=\"twelve columns\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;\" align=\"left\" valign=\"top\"><h2 style=\"text-align: center; font-family: Raleway,Helvetica,Arial,sans-serif; color: #222222; font-weight: normal; line-height: 1.3; word-break: normal; font-size: 36px; margin: 0; padding: 0;\" align=\"center\">"
				+ "Bienvenue sur Eza!</h2><br /><p style=\"text-align: center; font-family: Raleway,Helvetica,Arial,sans-serif; font-size: 15px; color: #222222; font-weight: normal; line-height: 19px; margin: 0 0 10px; padding: 0;\" align=\"center\">"
				+ message
				+ "</p></td><td class=\"expander\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\" align=\"left\" valign=\"top\"></td></tr></table><center style=\"width: 100%; min-width: 580px;\"><table class=\"button medium-button round\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 50%; overflow: hidden; padding: 0; margin: 0 auto;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #ffffff; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; display: block; width: auto !important; -webkit-border-radius: 500px; -moz-border-radius: 500px; border-radius: 500px; background: #2ba6cb; margin: 0; padding: 12px 0 10px; border: 1px solid #2284a1;\" align=\"center\" bgcolor=\"#1eaedb\" valign=\"top\"><a style=\"font-family: Raleway,Helvetica,Arial,sans-serif; color: #ffffff; text-decoration: none; font-weight: bold; font-size: 20px;\" "
				+ "href=\"" + url + "\">" + urlText
				+ "</a></td></tr></table></center></td></tr></table><br /><table class=\"row\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td class=\"wrapper last\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 10px 0px 0px;\" align=\"left\" valign=\"top\"><table class=\"twelve columns\" style=\"border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;\"><tr style=\"vertical-align: top; text-align: left; padding: 0;\" align=\"left\"><td align=\"center\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;\" valign=\"top\"><center style=\"width: 100%; min-width: 580px;\"><p style=\"text-align: center; font-family: Raleway,Helvetica,Arial,sans-serif; color: #222222; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0 0 10px; padding: 0;\" align=\"center\"><a href=\"#\" style=\"color: #2ba6cb; text-decoration: none;\">Conditions d'utilisation</a> | Copyright © Eza Media, Inc 2016</p></center></td><td class=\"expander\" style=\"word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica','Arial',sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;\" align=\"left\" valign=\"top\"></td></tr></table></td></tr></table></td></tr></table></center></td></tr></table></body></html>";

		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		MimeMessageHelper mimeMessageHelper;
		try {
			mimeMessageHelper = new MimeMessageHelper(mimeMessage, false, "utf-8");
			mimeMessage.setContent(customMessage, "text/html");
			mimeMessageHelper.setTo(recipientAddress);
			mimeMessageHelper.setFrom("emoleumassi@yahoo.com");
			mimeMessageHelper.setSubject(subject);
		} catch (MessagingException e1) {
			LOGGER.error(e1.getMessage());
		}

		Thread thread = new Thread() {
			public void run() {
				try {
					javaMailSender.send(mimeMessage);
				} catch (MailParseException | MailSendException e) {
					LOGGER.error(e.getMessage());
				}
			}
		};
		thread.start();
	}
}
