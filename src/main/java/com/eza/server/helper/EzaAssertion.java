package com.eza.server.helper;

import java.util.Calendar;

import com.eza.server.model.RegistrationToken;
import com.eza.server.model.User;

/**
 * 
 * @author emoleumasi
 *
 */
@SuppressWarnings("ALL")
public class EzaAssertion {

	/**
	 * validate an UUID with regex. throw an exception if the uuid doesn't
	 * conform
	 */
	public static void assertValidUUID(String uuid) {
		String regex = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
		boolean isValid = uuid.trim().matches(regex);
		if (!isValid)
			throw new IllegalArgumentException(EzaError.UUID_ERROR);
	}

	/**
	 * assert than the verification token's time isn't expired
	 * 
	 * @param expirationTime
	 */
	public static void assertRegistrationNotExpired(RegistrationToken registrationToken) {

		Calendar calendar = Calendar.getInstance();
		long actuelTime = calendar.getTime().getTime();
		long expirationTime = registrationToken.getExpirationDate().getTime();
		if ((expirationTime - actuelTime) <= 0)
			throw new IllegalArgumentException(EzaError.TIME_EXPIRED_ERROR);
	}

	/**
	 * throw an exception if the user account isn't enabled
	 * 
	 * @param object
	 */
	public static void assertUserEnabled(User user) {
		if (!user.isEnabled())
			throw new IllegalArgumentException(EzaError.USER_NOT_ENABLED_ERROR);
	}
	
	/**
	 * throw an exception if the user account is enabled
	 * 
	 * @param object
	 */
	public static void assertUserNotEnabled(User user) {
		if (user.isEnabled())
			throw new IllegalArgumentException(EzaError.USER_ALREADY_ENABLED_ERROR);
	}
}
