package com.eza.server.helper;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by emoleumassi on 24.08.16.
 */
@Service("EzaEncryptor")
public class EzaEncryptor {

    private ConfigurablePasswordEncryptor configurablePasswordEncryptor;

    @PostConstruct
    public void init(){
        configurablePasswordEncryptor = new ConfigurablePasswordEncryptor();
        configurablePasswordEncryptor.setAlgorithm("SHA-1");
    }

    public String encryptPassword(String password){
        return configurablePasswordEncryptor.encryptPassword(password);
    }

    public boolean checkPassword(String inputPassword, String encryptedPassword){
        return configurablePasswordEncryptor.checkPassword(inputPassword, encryptedPassword);
    }
}
