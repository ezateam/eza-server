package com.eza.server.helper;

/**
 * 
 * @author emoleumassi
 *
 */
public final class EzaError {

	// ERROR_CODES
	private static final String UUID = "EZA_001 ";
	private static final String USER_LOGIN = "EZA_002 ";
	private static final String USER_ALREADY_ENABLED = "EZA_003 ";
	private static final String TIME_EXPIRED = "EZA_004 ";
	private static final String USER_NOT_ENABLED = "EZA_005 ";
	private static final String NOT_ENTITY_FOUND = "EZA_006 ";
	private static final String ENTITY_ALREADY_EXISTS = "EZA_007 ";
	private static final String NO_SEASON_TITLE = "EZA_008 ";
	private static final String API_KEY_LOGIN = "EZA_009 ";
	
	public static final String UUID_ERROR = UUID + "This uuid is not a valid uuid.\n";
	public static final String USER_LOGIN_ERROR = USER_LOGIN + "Check the username and password please.\n";
	public static final String USER_ALREADY_ENABLED_ERROR = USER_ALREADY_ENABLED + "The user is already enabled.\n";
	public static final String USER_NOT_ENABLED_ERROR = USER_NOT_ENABLED + "This User isn't enabled.\n";
	public static final String TIME_EXPIRED_ERROR = TIME_EXPIRED + "The verificationToken is already expired.\n";
	public static final String NOT_ENTITY_FOUND_ERROR = NOT_ENTITY_FOUND + "The aren't object for this request.\n";
	public static final String ENTITY_ALREADY_EXISTS_ERROR = ENTITY_ALREADY_EXISTS + "The entity already exists.\n";
	public static final String NO_SEASON_TITLE_ERROR = NO_SEASON_TITLE + "There client has to set a season title.\n";
	public static final String API_KEY_LOGIN_ERROR = API_KEY_LOGIN + "Check the eza api key please.\n";
}
