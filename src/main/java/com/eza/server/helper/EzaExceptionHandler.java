package com.eza.server.helper;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;

import com.eza.server.exception.LoginException;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.DisabledException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author emoleumassi
 */
@ControllerAdvice
@SuppressWarnings("unchecked")
public class EzaExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    ResponseHelper responseHelper;

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public ResponseEntity<String> illegalArgumentException(IllegalArgumentException exception)
            throws Exception {
        return responseHelper.currentResponse(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EmptyResultDataAccessException.class})
    @ResponseBody
    public ResponseEntity<String> emptyResultDataAccessException(EmptyResultDataAccessException exception)
            throws Exception {
        return responseHelper.currentResponse(EzaError.NOT_ENTITY_FOUND_ERROR + exception.getMessage(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NoResultException.class})
    @ResponseBody
    public ResponseEntity<String> noResultException(NoResultException exception) throws Exception {
        return responseHelper.currentResponse(EzaError.NOT_ENTITY_FOUND_ERROR + exception.getMessage(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseBody
    public ResponseEntity<String> dataIntegrityViolationException(DataIntegrityViolationException exception)
            throws Exception {
        return responseHelper.currentResponse(EzaError.ENTITY_ALREADY_EXISTS_ERROR + exception.getMessage(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EntityExistsException.class})
    @ResponseBody
    public ResponseEntity<String> entityExistsException(EntityExistsException exception) throws Exception {
        return responseHelper.currentResponse(EzaError.ENTITY_ALREADY_EXISTS_ERROR + exception.getMessage(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EncryptionOperationNotPossibleException.class})
    @ResponseBody
    public ResponseEntity<String> encryptionOperationNotPossibleException(EncryptionOperationNotPossibleException exception) throws Exception {
        return responseHelper.currentResponse(EzaError.USER_LOGIN_ERROR + exception.getMessage(),
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({DisabledException.class})
    @ResponseBody
    public ResponseEntity<String> disabledException(DisabledException exception) throws Exception {
        return responseHelper.currentResponse(exception.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({LoginException.class})
    @ResponseBody
    public ResponseEntity<String> loginException(LoginException exception) throws Exception {
        return responseHelper.currentResponse(exception.getMessage(), HttpStatus.UNAUTHORIZED);
    }
}
