package com.eza.server.helper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import org.springframework.util.Base64Utils;

/**
 * 
 * @author emoleumassi
 * 
 */
@SuppressWarnings("ALL")
public class EzaHelper {

	// 10 Minutes expiration time
	private static final int EXPIRATION = 10;

	private final static UUID createUUID() {
		return UUID.randomUUID();
	}

	/**
	 * generate a new UUID as String.
	 * 
	 * @return
	 */
	public final static String newUUID() {
		return String.valueOf(createUUID());
	}

	/**
	 * validate an UUID with regex.
	 * 
	 * @return
	 */
	public static boolean validateEmail(String email) {
		String regex = "^(.+)@(.+)$";
		return email.trim().matches(regex);
	}

	/**
	 * Create an new verificationToken and encode it in 64 bits.
	 * 
	 * @return
	 */
	public static String createVerificationToken() {
		String uuid = newUUID() + ":" + newUUID();
		return new String(Base64Utils.encode(uuid.getBytes()));
	}

	/**
	 * 
	 * @param expiryTimeInMinutes
	 * @return
	 */
	public static final Date calculateExpiryDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Timestamp(calendar.getTime().getTime()));
		calendar.add(Calendar.MINUTE, EXPIRATION);
		return new Date(calendar.getTime().getTime());
	}

	public static final String randomString(int length) {
		String text = "abcdefghijklmnopqrstuvwxyz0987654321ABCDEFGHIJKLMNOPQRSTUVWXZ";
		char[] chars = text.toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();

	}
}
