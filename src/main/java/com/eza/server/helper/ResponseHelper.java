package com.eza.server.helper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * 
 * @author emoleumassi
 *
 */
@Service("responseHelper")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ResponseHelper {

	/**
	 * Create a response for the HTTP-Request.
	 */
	public ResponseEntity currentResponse(Object message, HttpStatus httpStatus) {
		return new ResponseEntity(message, httpStatus);
	}
}
