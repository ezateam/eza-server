package com.eza.server.service;

import org.springframework.stereotype.Repository;

import com.eza.server.model.User;

/**
 * 
 * @author emoleumassi
 *
 */
@Repository
public interface UserService { 

	User findByUsername(String username);
	User findByEmailOrName(String login);
	String findUsernameByEmail(String username);
	int enabledUser(User user);
	int resetPassword(String username, String password);
	
}
