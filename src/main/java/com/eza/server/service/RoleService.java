package com.eza.server.service;

import com.eza.server.model.Role;

/**
 * 
 * @author emoleumassi
 *
 */
public interface RoleService {

	Role findByRolename(String rolename);
}
