package com.eza.server.service;

import java.util.List;

/**
 * 
 * @author emoleumassi
 *
 */
public interface GlobalEntityManagerService<T> {

	Object createEntity(T t);
	void removeEntity(T t);
	List<T> getAll();
	void setClazz(final Class<T> clazzToSet);
	T findEntityByParameter(String parameter, String value);
	Long countRecords();
}
