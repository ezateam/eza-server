package com.eza.server.bootstrap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Configuration
@Service("propertyConfiguration")
public class PropertyConfiguration implements InitializingBean {

	@Value("${eza.server.security.password.id}")
	private String passwordId;

	@Value("${eza.server.security.password.secret}")
	private String passwordSecret;

	@Value("${eza.server.security.password.authorization}")
	private String passwordAuthorization;

	@Value("${eza.server.security.client.credentials.id}")
	private String clientCredentialsId;

	@Value("${eza.server.security.client.credentials.secret}")
	private String clientCredentialsSecret;

	@Value("${eza.server.security.client.credentials.authorization}")
	private String clientCredentialsAuthorization;

	@Value("${eza.server.host}")
	private String ezaHost;
	
	@Value("${eza.server.ipaddress}")
	private String ezaIPAddress;
	
	public String getPasswordId() {
		return passwordId;
	}

	public String getPasswordSecret() {
		return passwordSecret;
	}

	public String getPasswordAuthorization() {
		return passwordAuthorization;
	}

	public String getClientCredentialsId() {
		return clientCredentialsId;
	}

	public String getClientCredentialsSecret() {
		return clientCredentialsSecret;
	}

	public String getClientCredentialsAuthorization() {
		return clientCredentialsAuthorization;
	}

	public String getEzaHost() {
		return ezaHost;
	}
	
	public String getEzaIPAddress() {
		return ezaIPAddress;
	}

	public void afterPropertiesSet() throws Exception {}
}