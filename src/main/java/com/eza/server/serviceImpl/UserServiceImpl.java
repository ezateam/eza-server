package com.eza.server.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TransactionRequiredException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eza.server.model.User;
import com.eza.server.service.UserService;

/**
 * 
 * @author emoleumassi
 *
 */
@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User findByUsername(String username) throws EmptyResultDataAccessException {
		String query = "FROM User u WHERE u.name = :username";
		return (User) entityManager.createQuery(query).setParameter("username", username).getSingleResult();
	}

	@Override
	public String findUsernameByEmail(String email) throws EmptyResultDataAccessException {
		String query = "SELECT u.name FROM User u WHERE u.email = :email";
		return (String) entityManager.createQuery(query).setParameter("email", email).getSingleResult();
	}

	@Override
	public User findByEmailOrName(String login) throws EmptyResultDataAccessException {
		String query = "FROM User u WHERE u.email = :email OR u.name = :name";
		return (User) entityManager.createQuery(query).setParameter("email", login).setParameter("name", login)
				.getSingleResult();
	}

	@Override
	public int enabledUser(User user) throws TransactionRequiredException {
		String query = "UPDATE User u set u.enabled = 1 WHERE u.userUUId = :userUUId";
		int result = 0;
		try {
			result = entityManager.createQuery(query).setParameter("userUUId", user.getUserUUId()).executeUpdate();
		} catch (IllegalArgumentException | PersistenceException e) {
			LOGGER.error(e.getMessage());
		}
		return result;
	}

	@Override
	public int resetPassword(String username, String password) {
		String query = "UPDATE User u set u.password = :password WHERE u.name = :username";
		int result = 0;
		try {
			result = entityManager.createQuery(query).setParameter("username", username)
					.setParameter("password", password).executeUpdate();
		} catch (TransactionRequiredException e) {
			LOGGER.error(e.getMessage());
		}
		return result;
	}
}
