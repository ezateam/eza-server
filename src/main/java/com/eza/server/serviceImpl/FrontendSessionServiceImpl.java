package com.eza.server.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eza.server.model.FrontendSession;

/**
 * 
 * @author emoleumassi
 *
 */
@Transactional
@Service("frontendSessionService")
public class FrontendSessionServiceImpl {

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = LoggerFactory.getLogger(FrontendSessionServiceImpl.class);

	public FrontendSession read(String sessionId) throws EmptyResultDataAccessException {
		String query = "FROM FrontendSession fs WHERE fs.sessionUUId = :sessionUUId";
		return (FrontendSession) entityManager.createQuery(query).setParameter("sessionUUId", sessionId)
				.getSingleResult();
	}

	public int write(String sessionId, String data) {
		String query = "UPDATE FrontendSession fs set fs.data = :data WHERE fs.sessionUUId = :sessionUUId";
		int result = 0;
		try {
			result = entityManager.createQuery(query).setParameter("sessionUUId", sessionId)
					.setParameter("data", data).executeUpdate();
		} catch (IllegalArgumentException | PersistenceException e) {
			LOGGER.error(e.getMessage());
		}
		return result;
	}

	public int destroy(String sessionId) {
		String query = "DELETE FrontendSession fs WHERE fs.sessionUUId = :sessionId";
		int result = 0;
		try {
			result = entityManager.createQuery(query).setParameter("sessionId", sessionId).executeUpdate();
		} catch (IllegalArgumentException | PersistenceException e) {
			LOGGER.error(e.getMessage());
		}
		return result;
	}

	public int clean(int maxLifetime) {
		String query = "DELETE FrontendSession fs WHERE fs.lifetime >= :lifetime";
		int result = 0;
		try {
			result = entityManager.createQuery(query).setParameter("lifetime", maxLifetime).executeUpdate();
		} catch (IllegalArgumentException | PersistenceException e) {
			LOGGER.error(e.getMessage());
		}
		return result;
	}
}
