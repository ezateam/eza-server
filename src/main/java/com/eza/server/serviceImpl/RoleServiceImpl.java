package com.eza.server.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.RollbackException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eza.server.model.Role;
import com.eza.server.service.RoleService;

/**
 * 
 * @author emoleumassi
 *
 */
@Transactional
@Service("roleService")
public class RoleServiceImpl implements RoleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);

	@PersistenceContext
	private EntityManager entityManager;	
	
	@Override
	public Role findByRolename(String rolename) {
		try {
			String query = "FROM Role r WHERE r.name = :rolename";
			return (Role) entityManager.createQuery(query).setParameter("rolename", rolename).getSingleResult();	
		} catch (SecurityException | IllegalStateException | RollbackException e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
}
