package com.eza.server.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.eza.server.model.Season;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author emoleumassi
 *
 */
@Transactional
@Service("movieServiceImpl")
public class MovieServiceImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(MovieServiceImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	public Season findMoviesByTitle(String serieValue, String seasonValue) throws NoResultException {
			String query = "SELECT se FROM Serie s, Season se, Movie m, MovieTranslation mt " +
                    "WHERE s.title = :serieValue AND se.title = :seasonValue AND se.serie.id = s.serieUUId";
			return entityManager.createQuery(query, Season.class).setParameter("serieValue", serieValue)
                    .setParameter("seasonValue", seasonValue).getSingleResult();
	}
}
