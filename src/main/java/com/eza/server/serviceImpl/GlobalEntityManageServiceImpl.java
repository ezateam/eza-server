package com.eza.server.serviceImpl;

import java.util.List;

import javax.persistence.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eza.server.service.GlobalEntityManagerService;

/**
 * @author emoleumassi
 */
@Transactional
@Service("globalEntityManagerService")
@SuppressWarnings("unchecked")
public class GlobalEntityManageServiceImpl<T> implements GlobalEntityManagerService<T> {

    private Class<T> clazz;
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalEntityManageServiceImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void setClazz(final Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    @Override
    public Object createEntity(T t) throws DataIntegrityViolationException {
        try {
            entityManager.persist(t);
            LOGGER.info(t.toString());
            return t;
        } catch (IllegalArgumentException | EntityExistsException | TransactionRequiredException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void removeEntity(T t) {
        try {
            entityManager.remove(t);
            LOGGER.info(t.toString());
        } catch (IllegalArgumentException | TransactionRequiredException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public List<T> getAll() throws EmptyResultDataAccessException {
        String query = "FROM " + clazz.getName();
        return (List<T>) entityManager.createQuery(query).getResultList();
    }

    @Override
    public T findEntityByParameter(String parameter, String value) throws NoResultException {
        String query = "FROM " + clazz.getName() + " WHERE " + parameter + " = :value";
        return (T) entityManager.createQuery(query).setParameter("value", value).getSingleResult();
    }

    @Override
    public Long countRecords() throws EmptyResultDataAccessException {
        String query = "SELECT count(*) FROM " + clazz.getName();
        return (Long) entityManager.createQuery(query).getSingleResult();
    }
}
