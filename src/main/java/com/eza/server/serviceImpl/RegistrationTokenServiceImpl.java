package com.eza.server.serviceImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eza.server.model.RegistrationToken;

/**
 * 
 * @author emoleumassi
 *
 */
@Transactional
@Service("registrationTokenService")
public class RegistrationTokenServiceImpl {

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationTokenServiceImpl.class);

	public RegistrationToken getVerificationToken(String verificatioToken) throws EmptyResultDataAccessException {
		String query = "FROM RegistrationToken rt WHERE rt.verificationToken = :verificationToken "
				+ "AND rt.verificationToken != :dummyverificationtoken";
		return (RegistrationToken) entityManager.createQuery(query).setParameter("verificationToken", verificatioToken)
				.setParameter("dummyverificationtoken", "dummyverificationtoken")
				.getSingleResult();
	}

	public int updateVerificationToken(RegistrationToken registrationToken) {

		String query = "UPDATE RegistrationToken rt set rt.verificationToken = :verificationToken, "
				+ "rt.expirationDate = :expirationDate WHERE rt.user.id = :userUUId";
		int result = 0;
		try {
			result = entityManager.createQuery(query)
					.setParameter("userUUId", registrationToken.getUser().getUserUUId())
					.setParameter("verificationToken", registrationToken.getVerificationToken())
					.setParameter("expirationDate", registrationToken.getExpirationDate()).executeUpdate();
		} catch (IllegalArgumentException | PersistenceException e) {
			LOGGER.error(e.getMessage());
		}
		return result;
	}

	public RegistrationToken getRegistrationTokenByIdentify(String identify) throws EmptyResultDataAccessException {
		String query = "FROM RegistrationToken rt WHERE rt.user.name = :name OR rt.user.email = :email";
		return (RegistrationToken) entityManager.createQuery(query).setParameter("name", identify)
				.setParameter("email", identify).getSingleResult();
	}

}
