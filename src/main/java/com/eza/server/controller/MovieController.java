package com.eza.server.controller;

import com.eza.server.controller.helper.MovieControllerHelper;
import com.eza.server.helper.EzaAssertion;
import com.eza.server.helper.EzaError;
import com.eza.server.model.Movie;
import com.eza.server.model.MovieTranslation;
import com.eza.server.model.Season;
import com.eza.server.model.Serie;
import com.eza.server.serviceImpl.MovieServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author emoleumassi
 * @version 0.0.1-Alpha
 */
@Component
@RestController
@SuppressWarnings({"rawtypes", "unchecked"})
@RequestMapping(value = "/movies", produces = "application/json")
@Api(value = "/movies", description = "Endpoint for the movies management")
public class MovieController extends AbstractController {

    @Autowired
    MovieServiceImpl movieServiceImpl;
    @Autowired
    MovieControllerHelper movieControllerHelper;

    @PostConstruct
    public void init() {
        globalEntityManagerService.setClazz(Serie.class);
    }

    @ApiOperation(value = "return all the movies from the database")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "a list of series with seasons, movies and movie's translation"),
            @ApiResponse(code = 404, message = "EZA_006: no entries in the database")
    })
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Serie> getMovies() {
        return getAll();
    }

    @ApiOperation(value = "create a new serie")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "", response = Serie.class),
            @ApiResponse(code = 400, message = "EZA_007: this entity already exists. " +
                    "The combination serie.title, season.title and movieTranslation.title exists only once.")
    })
    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Serie> createMovie(@RequestBody @Valid Serie serie) {
        globalEntityManagerService.createEntity(serie);
        movieControllerHelper.createSeasons(serie, null);
        return responseHelper.currentResponse(serie, HttpStatus.OK);
    }

    @ApiOperation(value = "add a season to an existing serie")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "", response = Season.class),
            @ApiResponse(code = 400, message = "EZA_007: this entity already exists. " +
                    "The combination serie.title, season.title and movieTranslation.title exists only once.")
    })
    @RequestMapping(value = "/createSeason/{title}", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Season> createSeason(@PathVariable("title") String title, @RequestBody @Valid Season season) {
        Serie serie = (Serie) findEntityByParameter("title", title);
        movieControllerHelper.createSeasons(serie, season);
        return responseHelper.currentResponse(season, HttpStatus.OK);
    }

    @ApiOperation(value = "add a movie to an existing season")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "", response = Movie.class),
            @ApiResponse(code = 400, message = "EZA_007: this entity already exists. " +
                    "The combination serie.title, season.title and movieTranslation.title exists only once.")
    })
    @RequestMapping(value = "/createMovie/{serietitle}/{seasontitle}", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Movie> createMovie(@PathVariable("serietitle") String serieTitle, @PathVariable("seasontitle") String seasonTitle, @RequestBody @Valid Movie movie) {
        Season season = movieServiceImpl.findMoviesByTitle(serieTitle, seasonTitle);
        movieControllerHelper.createMovies(season, movie);
        return responseHelper.currentResponse(movie, HttpStatus.OK);
    }

    @ApiOperation(value = "find serie/season/movie by title. " +
            "Serie' title is required while season' and movietranslation's title isn't required.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "By success", response = Serie.class),
            @ApiResponse(code = 400, message = "EZA_008: no set a season title when the client set the movie's title"),
            @ApiResponse(code = 404, message = "EZA_006: no entries in the database for the serie/season or movie's title")
    })
    @RequestMapping(value = "/{serietitle}", method = RequestMethod.GET)
    public ResponseEntity<Serie> findMovieByTitle(@PathVariable("serietitle") String serieTitle,
                                                   @RequestParam(value = "seasontitle", required = false) String seasonTitle,
                                                   @RequestParam(value = "movietranslationtitle", required = false) String movieTranslationTitle) {
        if(seasonTitle == null && movieTranslationTitle != null)
            throw new IllegalArgumentException(EzaError.NO_SEASON_TITLE_ERROR);

        globalEntityManagerService.setClazz(Serie.class);
        Serie serie = (Serie) findEntityByParameter("title", serieTitle);
        if(seasonTitle != null) {
            Season season = movieControllerHelper.findSeasonInSerie(serie.getSeasons(), currentSeason -> currentSeason.getTitle().equalsIgnoreCase(seasonTitle));
            if(movieTranslationTitle != null){
                Movie movie = movieControllerHelper.findMovieTitleInSeason(season, movieTranslationTitle);
                return responseHelper.currentResponse(movie, HttpStatus.OK);
            }
            return responseHelper.currentResponse(season, HttpStatus.OK);
        }
        return responseHelper.currentResponse(serie, HttpStatus.OK);
    }

    @ApiOperation(value = "{records} is the current number in the client. " +
            "This method is a helper to check if the are new entries in the database")
    @ApiResponse(code = 200, message = "true if the database is updated else false", response = Boolean.class)
    @RequestMapping(value = "/newrecords/{records}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkNewRecords(@PathVariable Long records) {
        Long oldNumberRecord = globalEntityManagerService.countRecords();
        if (records != 0 && Objects.equals(records, oldNumberRecord))
            return responseHelper.currentResponse(false, HttpStatus.OK);
        return responseHelper.currentResponse(true, HttpStatus.OK);
    }

    @ApiOperation(value = "remove a movie by uuid")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "by success", response = Serie.class),
            @ApiResponse(code = 404, message = "EZA_006: no entries with this UUID in the database"),
            @ApiResponse(code = 400, message = "EZA_001: no valid UUID")
    })
    @RequestMapping(value = "/delete/{uuid}", method = RequestMethod.DELETE)
    public ResponseEntity<Serie> deleteMovieByID(@PathVariable String uuid) {
        EzaAssertion.assertValidUUID(uuid);
        return deleteByParameter("id", uuid);
    }
}
