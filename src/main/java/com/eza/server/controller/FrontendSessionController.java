package com.eza.server.controller;

import javax.validation.Valid;

import com.eza.server.serviceImpl.FrontendSessionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eza.server.model.FrontendSession;

/**
 * 
 * @author emoleumassi
 * @version 0.0.1-Alpha
 *
 */
@Component
@RestController
@SuppressWarnings("unchecked")
@RequestMapping(value = "/frontendSession", produces = "application/json")
public class FrontendSessionController extends AbstractController<FrontendSession> {

	@Autowired
	FrontendSessionServiceImpl frontendSessionService;

	@RequestMapping()
	public ResponseEntity<FrontendSession> getFrontendSessions() {
		globalEntityManagerService.setClazz(FrontendSession.class);
		return getAll();
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> create(@RequestBody(required = true) @Valid FrontendSession frontendSession) {
		globalEntityManagerService.createEntity(frontendSession);
		return responseHelper.currentResponse("Successful create new frontendSession", HttpStatus.OK);
	}

	@RequestMapping(value = "/read/{sessionId}")
	public ResponseEntity<?> read(@PathVariable String sessionId) {
		FrontendSession frontendSession = frontendSessionService.read(sessionId);
		return responseHelper.currentResponse(frontendSession, HttpStatus.OK);
	}

	@RequestMapping(value = "/write/{sessionId}", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
	public ResponseEntity<?> write(@PathVariable String sessionId, @RequestParam("data") String data) {
		frontendSessionService.write(sessionId, data);
		return responseHelper.currentResponse("Successful write", HttpStatus.OK);
	}

	@RequestMapping(value = "/destroy/{sessionId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> destroy(@PathVariable String sessionId) {
		frontendSessionService.destroy(sessionId);
		return responseHelper.currentResponse("Successfull delete", HttpStatus.OK);
	}

	@RequestMapping(value = "/destroybymaxlifetime/{maxLifetime}", method = RequestMethod.DELETE)
	public ResponseEntity<?> clear(@PathVariable int maxLifetime) {
		frontendSessionService.clean(maxLifetime);
		return responseHelper.currentResponse("Successfull delete", HttpStatus.OK);
	}
}
