package com.eza.server.controller;

import java.util.*;

import javax.validation.Valid;

import com.eza.server.exception.LoginException;
import com.eza.server.helper.EzaEncryptor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.DisabledException;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eza.server.bootstrap.PropertyConfiguration;
import com.eza.server.helper.EzaError;
import com.eza.server.helper.ResponseHelper;
import com.eza.server.model.User;
import com.eza.server.security.EndpointsAuthentification;
import com.eza.server.service.UserService;

/**
 * 
 * @author emoleumassi
 *
 */
@Component
@RestController
@RequestMapping(value = "/login", produces = "application/json")
@Api(value = "/login", description = "Endpoint for the login management")
@SuppressWarnings("unchecked")
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	UserService userService;
	@Autowired
	EzaEncryptor ezaEncryptor;
	@Autowired
	ResponseHelper responseHelper;
	@Autowired
	EndpointsAuthentification endpointsAuthentification;

	private User user;
	private String ezaApiKey;
	@Autowired
	PropertyConfiguration propertyConfiguration;

	@ApiOperation(value = "Login the user by email/name and password")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "By success the user receive an access token"),
			@ApiResponse(code = 404, message = "EZA_006: the aren't user with this name/email."),
			@ApiResponse(code = 401, message = "EZA_002: the username/email doesn't match with password, " +
					"EZA_005: The user account isn't enabled, " +
					"EZA_009: the eza-apikey isn't correct.")
	})
	@RequestMapping(method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
	public ResponseEntity<String> login(@RequestHeader("eza-apikey") String apikey,
			@RequestParam("username") @Valid @NotBlank String username,
			@RequestParam("password") @Valid @NotBlank String password) {

		user = userService.findByEmailOrName(username.toLowerCase());

		if (!ezaEncryptor.checkPassword(password, user.getPassword()))
			throw new LoginException(EzaError.USER_LOGIN_ERROR);

		ezaApiKey = apikey;
		ResponseEntity<String> responseEntity = performLogin();
		handleResponseEntitiy(responseEntity);
		return responseHelper.currentResponse(responseEntity.getBody(), responseEntity.getStatusCode());
	}

	private ResponseEntity<String> performLogin(){
		Map<String, String> data = new HashMap<>();
		data.put("username", user.getName());
		data.put("password", user.getPassword());
		data.put("grant_type", "password");

		HttpEntity<?> httpEntity = customHttpEntity(data);
		return endpointsAuthentification.login(httpEntity);
	}

	private HttpEntity<?> customHttpEntity(Map<String, String> data) {

		HttpHeaders httpHeaders = customHttpHeaders();
		MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
		data.forEach((k, v) -> multiValueMap.add(k, v));
		return new HttpEntity<Object>(multiValueMap, httpHeaders);
	}

	private HttpHeaders customHttpHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Basic " + ezaApiKey);
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		return httpHeaders;
	}

	private void handleResponseEntitiy(ResponseEntity<String> responseEntity){
		if (Objects.equals(responseEntity, null)) {
			if (!user.isEnabled())
				throw new DisabledException(EzaError.USER_NOT_ENABLED_ERROR);
			if (!ezaApiKey.equals(propertyConfiguration.getPasswordAuthorization()))
				throw new LoginException(EzaError.API_KEY_LOGIN_ERROR);
		}
	}
}
