package com.eza.server.controller;

import java.util.Date;
import java.util.List;

import com.eza.server.serviceImpl.RegistrationTokenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.eza.server.helper.EmailHelper;
import com.eza.server.helper.EmailHelper.EmailType;
import com.eza.server.helper.EzaAssertion;
import com.eza.server.helper.EzaHelper;
import com.eza.server.helper.ResponseHelper;
import com.eza.server.model.RegistrationToken;
import com.eza.server.model.User;
import com.eza.server.service.GlobalEntityManagerService;
import com.eza.server.service.UserService;

/**
 * 
 * @author emoleumassi
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class AbstractController<T> {

	@Autowired
	EmailHelper emailHelper;
	@Autowired
	UserService userService;
	@Autowired
	ResponseHelper responseHelper;
	@Autowired
	RegistrationTokenServiceImpl registrationTokenService;
	@Autowired
	GlobalEntityManagerService globalEntityManagerService;

	protected ResponseEntity<T> getAll() {
		List<T> t = globalEntityManagerService.getAll();
		return responseHelper.currentResponse(t, HttpStatus.OK);
	}

	protected ResponseEntity<T> findByParameter(String parameter, String value) {
		T t = findEntityByParameter(parameter, value);
		return responseHelper.currentResponse(t, HttpStatus.OK);
	}

	protected ResponseEntity<T> deleteByParameter(String parameter, String value) {
		T t = findEntityByParameter(parameter, value);
		globalEntityManagerService.removeEntity(t);
		return responseHelper.currentResponse(t, HttpStatus.OK);
	}

	protected ResponseEntity sendVerificationToken(String identify, EmailType emailType) {
		RegistrationToken registrationToken = registrationTokenService
				.getRegistrationTokenByIdentify(identify.toLowerCase());

		User user = registrationToken.getUser();
		EzaAssertion.assertUserEnabled(user);
		
		customRegistrationToken(registrationToken);
		registrationTokenService.updateVerificationToken(registrationToken);
		emailHelper.sentEmail(registrationToken, emailType);

		return responseHelper.currentResponse("Verification link will be send per email to user\n", HttpStatus.OK);
	}

	protected void customRegistrationToken(RegistrationToken registrationToken) {
		Date expirationDate = EzaHelper.calculateExpiryDate();
		String verificationToken = EzaHelper.createVerificationToken();
		registrationToken.setExpirationDate(expirationDate);
		registrationToken.setVerificationToken(verificationToken);
	}
	
	protected void updateVerificationToken(RegistrationToken registrationToken){
		registrationToken.setVerificationToken("dummyverificationtoken");
		registrationTokenService.updateVerificationToken(registrationToken);
	}
	
	protected T findEntityByParameter(String parameter, String value) {
		return (T) globalEntityManagerService.findEntityByParameter(parameter, value);
	}
}
