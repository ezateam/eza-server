package com.eza.server.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import com.eza.server.helper.EzaEncryptor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eza.server.helper.EmailHelper.EmailType;
import com.eza.server.helper.EzaAssertion;
import com.eza.server.model.RegistrationToken;
import com.eza.server.model.Role;
import com.eza.server.model.User;
import com.eza.server.model.noentities.RegistrationUser;
import com.eza.server.service.RoleService;

/**
 * 
 * @author emoleumassi
 *
 */

@Component
@RestController
@Api(value = "/registration", description = "Endpoint for the movies management")
@RequestMapping(value = "/registration", produces = "application/json")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class RegistrationController extends AbstractController {

	@Autowired
	RoleService roleService;
	@Autowired
	EzaEncryptor ezaEncryptor;

	@ApiOperation(value = "create a new user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "return a created user with UUID", response = User.class),
			@ApiResponse(code = 404, message = "EZA_007: the user with this name or email already exists")
	})
	@RequestMapping(value = "/newuser", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<User> registrerUser(@RequestBody(required = true) @Valid RegistrationUser registrationUser) {

		User user = createAndSendConfirmationEmail(registrationUser);
		return responseHelper.currentResponse(user, HttpStatus.OK);
	}

	@ApiOperation(value = "validate the verification token for the user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "a list of series with seasons, movies and movie's translation"),
			@ApiResponse(code = 400, message = "EZA_003: The user is already enabled or EZA_004: the verification token expired"),
			@ApiResponse(code = 404, message = "EZA_006: the verification token doesn't match")
	})
	@RequestMapping(value = "/confirmRegistration", method = RequestMethod.GET)
	public ResponseEntity<String> confirmRegistration(@RequestParam("verificationToken") String verificationToken) {

		RegistrationToken registrationToken = registrationTokenService.getVerificationToken(verificationToken);
		User user = registrationToken.getUser();
		EzaAssertion.assertUserNotEnabled(user);
		EzaAssertion.assertRegistrationNotExpired(registrationToken);
		userService.enabledUser(user);
		updateVerificationToken(registrationToken);
		return responseHelper.currentResponse("Successfull registration\n", HttpStatus.OK);
	}

	@ApiOperation(value = "Re-send an email with new verification token to user. " +
			"An identify is an email or a name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The user will receive  a new verification token per email"),
			@ApiResponse(code = 400, message = "EZA_003: The user is already enabled"),
			@ApiResponse(code = 404, message = "EZA_006: no entries in the database with this identify")
	})
	@RequestMapping(value = "/sendNewVerificationToken", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
	public ResponseEntity<String> sendNewVerificationToken(@RequestParam("identify") @NotBlank String identify) {

		RegistrationToken registrationToken = registrationTokenService
				.getRegistrationTokenByIdentify(identify.toLowerCase());
		EzaAssertion.assertUserNotEnabled(registrationToken.getUser());
		customRegistrationToken(registrationToken);
		registrationTokenService.updateVerificationToken(registrationToken);
		emailHelper.sentEmail(registrationToken, EmailType.NEW_USER);
		return responseHelper.currentResponse("New verificationtoken will send per email\n", HttpStatus.OK);
	}

	private User createAndSendConfirmationEmail(RegistrationUser registrationUser) {

		User user = createNewUser(registrationUser);
		globalEntityManagerService.createEntity(user);
		RegistrationToken registrationToken = new RegistrationToken();
		customRegistrationToken(registrationToken);
		registrationToken.setUser(user);
		globalEntityManagerService.createEntity(registrationToken);
		emailHelper.sentEmail(registrationToken, EmailType.NEW_USER);
		return user;
	}

	private User createNewUser(RegistrationUser registrationUser) {

		String roleName = registrationUser.getRole();
		String password = registrationUser.getPassword();
		password = ezaEncryptor.encryptPassword(password);
		String email = registrationUser.getEmail().toLowerCase();
		String username = registrationUser.getUsername().toLowerCase();

		Role role = roleService.findByRolename(roleName);
		User user = new User();
		user.setRole(role);
		user.setEmail(email);
		user.setPassword(password);
		user.setUsername(username);
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user.setRoles(roles);
		return user;
	}
}
