package com.eza.server.controller;

import com.eza.server.helper.EzaEncryptor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eza.server.helper.EmailHelper.EmailType;
import com.eza.server.helper.EzaAssertion;
import com.eza.server.model.RegistrationToken;
import com.eza.server.model.User;

/**
 * 
 * @author emoleumassi
 *
 */
@Component
@RestController
@RequestMapping(value = "/users", produces = "application/json")
@Api(value = "/users", description = "Endpoint for the user and password management")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class UserController extends AbstractController {

	@Autowired
	private EzaEncryptor ezaEncryptor;

	@ApiOperation(value = "return all the users from the database")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "a list of users"),
			@ApiResponse(code = 404, message = "EZA_006: no entries in the database")
			})
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<User> getUsers() {
		globalEntityManagerService.setClazz(User.class);
		return getAll();
	}

	@ApiOperation(value = "find the user by name or by email")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "An user with the email or name without password", response = User.class),
			@ApiResponse(code = 404, message = "EZA_006: no entries in the database with this email or name")
	})
	@RequestMapping(method = RequestMethod.GET, value = "/byemailorname/{name}/")
	public ResponseEntity<User> findUserByEmailORName(@PathVariable String name) {
		User user = userService.findByEmailOrName(name);
		user.setPassword("");
		return responseHelper.currentResponse(user, HttpStatus.OK);
	}

	@ApiOperation(value = "Send an email with verification token to user who resets his password. " +
			"An identify is an email or a name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The user will receive an email"),
			@ApiResponse(code = 400, message = "EZA_005: The user account isn't enabled"),
			@ApiResponse(code = 404, message = "EZA_006: no entries in the database with this identify")
	})
	@RequestMapping(value = "/password/forget", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
	public ResponseEntity<?> forgetUserPassword(@RequestParam("identify") @NotBlank String identify) {
		return sendVerificationToken(identify, EmailType.FORGET_PASSWORD);
	}

	@ApiOperation(value = "Handle the verification token")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "the verification token match"),
			@ApiResponse(code = 400, message = "EZA_005: The user account isn't enabled or EZA_004: the verification token expired"),
			@ApiResponse(code = 404, message = "EZA_006: the verification token doesn't match")
	})
	@RequestMapping(value = "/password/handleverificationtoken", method = RequestMethod.GET)
	public ResponseEntity<String> handlePasswordToken(@RequestParam("verificationToken") String verificationToken) {
		findRegistrationTokenByVerificationToken(verificationToken);
		return responseHelper.currentResponse("Successfull\n", HttpStatus.OK);
	}

	@ApiOperation(value = "Reset the user's password")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The user's password was successfully reset"),
			@ApiResponse(code = 400, message = "EZA_005: The user account isn't enabled or EZA_004: the verification token expired"),
			@ApiResponse(code = 404, message = "EZA_006: the verification token doesn't match")
	})
	@RequestMapping(value = "/password/reset", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
	public ResponseEntity<String> resetPassword(@RequestParam("verificationToken") String verificationToken,
			@RequestParam("password") String password) {
		RegistrationToken registrationToken = findRegistrationTokenByVerificationToken(verificationToken);
		String username = registrationToken.getUser().getName();
		password = ezaEncryptor.encryptPassword(password);
		userService.resetPassword(username, password);
		updateVerificationToken(registrationToken);
		return responseHelper.currentResponse("Successfull reset user password\n", HttpStatus.OK);
	}
	
	private RegistrationToken findRegistrationTokenByVerificationToken(String verificationToken) {
		RegistrationToken registrationToken = registrationTokenService.getVerificationToken(verificationToken);
		User user = registrationToken.getUser();
		EzaAssertion.assertUserEnabled(user);
		EzaAssertion.assertRegistrationNotExpired(registrationToken);
		return registrationToken;
	}
}
