package com.eza.server.controller.helper;

import com.eza.server.helper.EzaError;
import com.eza.server.model.*;
import com.eza.server.service.GlobalEntityManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author emoleumassi
 */
@Service("movieControllerHelper")
@SuppressWarnings("unchecked")
public class MovieControllerHelper {

    @SuppressWarnings("rawtypes")
    @Autowired
    GlobalEntityManagerService globalEntityManagerService;

    public void createSeasons(Serie serie, Season season) {
        if (season != null) {
            createNewSeason(serie, season);
        } else {
            serie.getSeasons().stream().forEach(eachSeason -> {
                createNewSeason(serie, eachSeason);
            });
        }
    }

    private void createCrews(Season season) {
        season.getCrews().stream().forEach(crew -> {
            crew.setSeason(season);
            globalEntityManagerService.createEntity(crew);
            createCrewTranslations(crew);
        });
    }

    protected void createCrewTranslations(Crew crew) {
        crew.getCrewTranslations().stream().forEach(crewTranslations -> {
            crewTranslations.setCrew(crew);
            globalEntityManagerService.createEntity(crewTranslations);
        });
    }

    public void createMovies(Season season, Movie movie) {
        if (movie != null) {
            createNewMovie(season, movie);
        } else {
            season.getMovies().stream().forEach(eachMovie -> {
                createNewMovie(season, eachMovie);
            });
        }
    }

    private void createMovieTranslations(Movie movie) {
        movie.getMovieTranslations().stream().forEach(movieTranslation -> {
            movieTranslation.setMovie(movie);
            globalEntityManagerService.createEntity(movieTranslation);
        });
    }

    private void createNewSeason(Serie serie, Season season) {
        season.setSerie(serie);
        globalEntityManagerService.createEntity(season);
        createCrews(season);
        createMovies(season, null);
    }

    private void createNewMovie(Season season, Movie movie) {
        movie.setSeason(season);
        globalEntityManagerService.createEntity(movie);
        createMovieTranslations(movie);
    }

    public Movie findMovieTitleInSeason(Season season, String movieTranslationTitle) {
        return season.getMovies()
                .stream()
                .flatMap(movie -> movie.getMovieTranslations().stream())
                .filter(movieTranslation -> movieTranslation.getTitle().equalsIgnoreCase(movieTranslationTitle.trim()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(EzaError.NOT_ENTITY_FOUND_ERROR))
                .getMovie();
    }

    public Season findSeasonInSerie(List<Season> seasons, Predicate<Season> seasonPredicate) {
        return seasons
                .stream()
                .filter(seasonPredicate)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(EzaError.NOT_ENTITY_FOUND_ERROR));
    }
}
