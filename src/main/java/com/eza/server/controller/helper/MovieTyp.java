package com.eza.server.controller.helper;

/**
 * Created by emoleumassi on 04.08.16.
 */
public enum MovieTyp {

    Film("film"), Serie("serie"), Season("season"), Movie("movie"), MovieTranslation("movietranslation");

    String value;

    private MovieTyp(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }
}
