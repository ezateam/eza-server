package com.eza.server.controller;

import java.util.List;

import com.eza.server.model.Serie;
import com.eza.server.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eza.server.model.Role;
import com.eza.server.service.GlobalEntityManagerService;
import com.eza.server.service.RoleService;

/**
 * 
 * @author emoleumassi
 *
 */
@Component
@RestController
@RequestMapping(value = "/roles", produces = "application/json")
@Api(value = "/roles", description = "Endpoint for the role management")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class RoleController {

	@Autowired
	RoleService roleService;
	@Autowired
	GlobalEntityManagerService globalEntityManagerService;

	@ApiOperation(value = "return all the roles from the database")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "a list of roles"),
			@ApiResponse(code = 404, message = "EZA_006: no entries in the database")
	})
	@RequestMapping(method = RequestMethod.GET)
	public List<Role> getRoles() {
		globalEntityManagerService.setClazz(Role.class);
		return globalEntityManagerService.getAll();
	}

	@ApiOperation(value = "create a new role")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "by success"),
			@ApiResponse(code = 404, message = "EZA_007: this role already exists")
	})
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = "application/json")
	public void createVideo(@RequestBody Role role) {
		globalEntityManagerService.createEntity(role);
	}

	@ApiOperation(value = "find a role by name")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "return the role with this role's name", response = Role.class),
			@ApiResponse(code = 404, message = "EZA_006: no entries in the database for this role's name")
	})
	@RequestMapping(value = "/{rolename}", method = RequestMethod.GET)
	public Role findRoleByName(@PathVariable String rolename) {
		return roleService.findByRolename(rolename);
	}

}
