INSERT IGNORE INTO role (name) VALUES ('ADMIN');
INSERT IGNORE INTO role (name) VALUES ('PRODUCER');
INSERT IGNORE INTO role (name) VALUES ('CUSTOMER');
INSERT IGNORE INTO teststreaming.user (id, email, enabled, username, password, role_id) VALUES ('d2026947-4628-abcd-zywx-39c2e74f0a82', 'emoleumassi@gmail.com', 1, 'emo', 'BgHjhftS8FkxS3oQ824RUwHm6r8ueLEtIXpYGw==', 3);
INSERT IGNORE INTO teststreaming.registration_token (id, expiration_date, verification_token, user_id) VALUES (uuid(), NOW(), 'YjgyOTk0MzYtMzgzOS00NWVhLTk2MTQtNTIwZGNiMjU2NWQ3OjE5OWU3MmVkLWQ1ZjktNDZiMS05OWIyLTc5MDRmZmM3NjM2YQ==', 'd2026947-4628-abcd-zywx-39c2e74f0a82');
INSERT IGNORE INTO teststreaming.user (id, email, enabled, username, password, role_id) VALUES ('45rz5gfg-5452-dvdf-fsdf-sf4243rdf545', 'usernoenabledtest@gmail.com', 0, 'usernoenabledtest', 'BgHjhftS8FkxS3oQ824RUwHm6r8ueLEtIXpYGw==', 3);
INSERT IGNORE INTO teststreaming.registration_token (id, expiration_date, verification_token, user_id) VALUES (uuid(), NOW(), 'AgtDWEerfg456SdssdsSDSDfgHjhftS8Fkx3oQ824RUwHm6ruetIXpfdfSA==', '45rz5gfg-5452-dvdf-fsdf-sf4243rdf545');
